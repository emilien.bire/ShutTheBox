using System.Collections;
using System.Collections.Generic;
using UnityEditor;
using UnityEngine;

public class TileOnMouseDetector : MonoBehaviour
{
    Camera cam;
    public float speed;
    void Start()
    {
        cam = Camera.main;
    }
    
    void Update()
    {
        Vector3 mousePos = cam.ScreenToWorldPoint(Input.mousePosition);
        transform.position = new Vector3(mousePos.x, mousePos.y, 0);
        //transform.Translate(new Vector3(Input.GetAxis("Horizontal") * speed * Time.deltaTime, Input.GetAxis("Vertical")*speed * Time.deltaTime,0));
    }
}
