using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class possibilityScript : MonoBehaviour
{
    public SpriteRenderer sprite;
    public GameObject[] tilesForPossibility;
    public bool isPossible = false;
    public bool isSelected = false;
    private bool isTriggered = false;
    private TextMesh textChild;


    void Start()
    {
        sprite = GetComponent<SpriteRenderer>();
        sprite.enabled = false;
        textChild = transform.GetChild(0).GetComponent<TextMesh>();
        textChild.gameObject.SetActive(false);
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.CompareTag("MouseObj"))
        {
            isTriggered = true;
            if (sprite.enabled)
            {
                sprite.color = Color.blue;

            }
        }
    }

    private void OnTriggerExit2D(Collider2D collision)
    {
        isTriggered = false;
        if (collision.CompareTag("MouseObj"))
        {
            if (sprite.enabled)
            {
                sprite.color = Color.white;
            }

        }
    }
    private void Update()
    {
 
    }

    public void ActivatePossibility(string possibilityText, GameObject[] tiles)
    {
        
        tilesForPossibility = new GameObject[tiles.Length];
        isPossible = true;
        sprite.enabled = true;
        textChild.gameObject.SetActive(true);
        tilesForPossibility = tiles;
        textChild.text = possibilityText;
    }

    public void SelectPossibility()
    {
        sprite.color = Color.green;
        foreach (GameObject tile in tilesForPossibility)
        {
            tile.GetComponent<TileScript>().Select();
        }
    }
    public void DeactivatePossibility()
    {
        isPossible = false;
        sprite.enabled = false;
        isSelected = false;
        textChild.gameObject.SetActive(false);
        sprite.color = Color.white;
        foreach (GameObject tile in tilesForPossibility)
        {
            tile.GetComponent<TileScript>().isSelected = false;
        }
    }
}
