using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TileScript : MonoBehaviour
{
    public int tileValue;
    public SpriteRenderer sprite;
    public bool isAvailable;
    public bool isSelected = false;
    private bool isTriggered = false;
    // Start is called before the first frame update

    private void Awake()
    {
        isAvailable = true;
    }
    void Start()
    {
       
        sprite = GetComponent<SpriteRenderer>();
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.CompareTag("MouseObj"))
        {
            isTriggered = true;
            if(!isSelected && isAvailable)
            {
                sprite.color = Color.blue;
                
            }
        }
    }

    private void OnTriggerExit2D(Collider2D collision)
    {
        isTriggered = false;
        if (collision.CompareTag("MouseObj"))
        {
            if(!isSelected && isAvailable)
            {
                sprite.color = Color.white;
            }
            
        }
    }
    private void Update()
    {
        if (isTriggered)
        {
            if (isAvailable)
            {
                if (Input.GetMouseButtonDown(0))
                {
                    Debug.Log("press");
                    if (isSelected)
                    {
                        isSelected = false;
                    }
                    else
                    {
                        sprite.color = Color.green;
                        isSelected = true;
                    }
                }
            }
        }

        
        else if(!isSelected && isAvailable)
        {
            sprite.color = Color.white;
        }
    }

    public void Lock()
    {
        isAvailable = false;
        isSelected = false;
        sprite.color = Color.grey;
    }
    public void ResetTile()
    {
        isAvailable = true;
        sprite.color = Color.white;
    }

    public void Select()
    {
        isSelected = true;
        sprite.color = Color.green;
        
    }
}
