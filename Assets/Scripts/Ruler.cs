using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEditor;
using UnityEditor.PackageManager.Requests;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Advertisements;

public class Ruler : MonoBehaviour
{
    //public bool[] availableTiles = new bool[9];
    public GameObject[] tiles;
    public GameObject[] possibilityTiles;
    public List<int[]> possibilities = new List<int[]>();
    public List<GameObject> currentlySelectedTiles = new List<GameObject>();
    public List<int> availableTilesInt = new List<int>();
    public int currentDicesValue;
    public static Ruler instance;

    public Text diceText;
    public Text warningText;
    public Text currentSolutionProposed;
    public Text possibilitiesText;

    private void Awake()
    {
        instance = this;
    }
    void Start()
    {
        foreach (GameObject tile in tiles)
        {
            TileScript script = tile.GetComponent<TileScript>();
            if(script.isAvailable)
            {
                availableTilesInt.Add(script.tileValue);
            }
            
        }
        //currentDicesValue = RollDices();
        //diceText.text = currentDicesValue.ToString();
    }

    public int RollDices()
    {
        int d1 = new int();
        int d2 = new int();
        if(availableTilesInt.Count == 1 && availableTilesInt[0] == 1)
        {
            d1 = UnityEngine.Random.Range(1, 7);
            d2 = 0;
        }
        else
        {
            d1 = UnityEngine.Random.Range(1, 7);
            d2 = UnityEngine.Random.Range(1, 7);
        }
        possibilities = GetPossibilitiesAll(d1+d2, availableTilesInt);
        string possibilitiesString = new string("");
        for (int i = 0; i < possibilities.Count; i++)
        {
            possibilitiesString = $"{string.Join("+", possibilities[i])}";
            List<GameObject> tileForPoss = new List<GameObject>();
            foreach (GameObject tile in tiles)
            {
                if (possibilities[i].Contains(tile.GetComponent<TileScript>().tileValue))
                {
                    tileForPoss.Add(tile);
                }
            }
            possibilityTiles[i].GetComponent<possibilityScript>().ActivatePossibility(possibilitiesString, tileForPoss.ToArray());
        }
        return d1 + d2;
    }

    public void RestartGame()
    {
        Debug.Log("Restarting game");
        availableTilesInt.Clear();
        foreach(GameObject tile in tiles)
        {

            TileScript script = tile.GetComponent<TileScript>();
            script.ResetTile();
            availableTilesInt.Add(script.tileValue);
        }

        //currentDicesValue = RollDices();
        diceText.text = currentDicesValue.ToString();
        currentSolutionProposed.text = "0";
    }

    public List<int[]> GetPossibilitiesAll(float output, List<int> availableInts)
    {
        List<int[]> listOfPossibilities = new List<int[]>();
        for (int i=0; i < availableInts.Count; i++)
        {
            if (availableInts[i] < output)
            {
                for(int j=0;j< availableInts.Count;j++)
                {
                    if (j>i && availableInts[i] + availableInts[j] < output)
                    {
                        for (int k = 0; k < availableInts.Count; k++)
                        {
                            if (k>j && k>i && availableInts[i] + availableInts[j] + availableInts[k] == output)
                            {
                                int[] possibility = new int[3];
                                possibility[0] = availableInts[i];
                                possibility[1] = availableInts[j];
                                possibility[2] = availableInts[k];
                                Array.Sort(possibility);
                                if (!listOfPossibilities.Contains(possibility))
                                {
                                    listOfPossibilities.Add(possibility);
                                }
                            }
                        }
                    }
                    else if (j>i && availableInts[i] + availableInts[j] == output)
                    {
                        int[] possibility = new int[2];
                        possibility[0] = availableInts[i];
                        possibility[1] = availableInts[j];
                        Array.Sort(possibility);
                        if (!listOfPossibilities.Contains(possibility))
                        {
                            listOfPossibilities.Add(possibility);
                        }
                    }
                }
            }
            else if(availableInts[i] == output)
            {
                int[] possibility = new int[1];
                possibility[0] = availableInts[i];
                if (!listOfPossibilities.Contains(possibility))
                {
                    listOfPossibilities.Add(possibility);
                }
            }
            

        }
        
        return listOfPossibilities;

    }

    public void ValidateTurn(float solutionSelected)
    {
        if (solutionSelected == currentDicesValue)
        {
            foreach (GameObject possTile in possibilityTiles)
            {
                possTile.GetComponent<possibilityScript>().DeactivatePossibility();
            }
            foreach (GameObject tile in currentlySelectedTiles)
            {
                TileScript script = tile.GetComponent<TileScript>();
                script.Lock();
                availableTilesInt.Remove(script.tileValue);
            }
        }
    }

}
