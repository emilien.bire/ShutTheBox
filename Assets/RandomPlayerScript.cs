using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEditor;
using UnityEditor.PackageManager.Requests;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Advertisements;

public class RandomPlayerScript : MonoBehaviour
{
    public int numberOfGames;
    private int iterationCounter;
    public int[] scores;
    public float averageScore;
    public float stdOfScore;
    public float numberOfWins;

    void Start()
    {
        scores = new int[numberOfGames];
    }

    void Update()
    {

        if (iterationCounter>= numberOfGames)
        {
            Time.timeScale = 0;
            double average = scores.Average();
            double sumOfSquaresOfDifferences = scores.Select(val => (val - average) * (val - average)).Sum();
            double sd = Math.Sqrt(sumOfSquaresOfDifferences / scores.Length);
            Debug.Log("Number of wins : " + numberOfWins.ToString() + " over " + numberOfGames + " games.");
            Debug.Log("Average score of :" + average.ToString() +"+-"+sd.ToString());
            this.enabled = false;
        }
        TakeTurn();
    }

    void LoseGame()
    {
        Debug.Log("Game lost");
        iterationCounter += 1;
        scores[iterationCounter-1] = Ruler.instance.availableTilesInt.Sum();
        Ruler.instance.RestartGame();
    }

    void TakeTurn()
    {
        
        //choose possibility if there is one
        Ruler.instance.currentDicesValue = Ruler.instance.RollDices();
        Ruler.instance.diceText.text = Ruler.instance.currentDicesValue.ToString();
        List<GameObject> possibilities = new List<GameObject>();
        foreach (GameObject possTile in Ruler.instance.possibilityTiles)
        {
            if(possTile.GetComponent<possibilityScript>().isPossible)
            {
                possibilities.Add(possTile);
            }
        }
        if (possibilities.Count == 0 && Ruler.instance.availableTilesInt.Count > 0)
        {
            LoseGame();
        }
        else if (Ruler.instance.availableTilesInt.Count == 0)
        {
            WinGame();
        }
        else
        {
            int p = UnityEngine.Random.Range(0, possibilities.Count);
            GameObject possibility = possibilities[p];
            possibility.GetComponent<possibilityScript>().SelectPossibility();
            Ruler.instance.currentlySelectedTiles.Clear();
            foreach (GameObject tile in Ruler.instance.tiles)
            {
                TileScript script = tile.GetComponent<TileScript>();
                if (script.isSelected)
                {
                    Ruler.instance.currentlySelectedTiles.Add(tile);
                }
            }
            int solutionSelected = 0;
            for (int i = 0; i < Ruler.instance.currentlySelectedTiles.Count; i++)
            {
                solutionSelected += Ruler.instance.currentlySelectedTiles[i].GetComponent<TileScript>().tileValue;
            }
            Ruler.instance.currentSolutionProposed.text = solutionSelected.ToString();
            //validate possibility
            Ruler.instance.ValidateTurn(Ruler.instance.currentDicesValue);
        }
        
    }
    
    void WinGame()
    {
        Debug.Log("Game win");
        iterationCounter += 1;
        numberOfWins += 1;
        scores[iterationCounter - 1] = 0;
        Ruler.instance.RestartGame();
    }
}
